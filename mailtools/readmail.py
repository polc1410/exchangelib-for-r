#! /usr/bin/env python
#
# This code is set to run as a cronjob and checks the server periodically for emails with CSV files attached to them in a subfolder called 'Submitted Data'
# It then saves the CSV in a folder on the server running the python (so that R can suck the data in at a later date)
# And moves the email to a Processed Data Folder
#
# This is VERY project specific.
# But might give an idea of how this might work. 
# 
# I'm not 100% sure how "secure" this is.  Exchange is already doing some filtering (email needs to be structured in a specific way, from a specific source)
# Its only handling CSV files
# R will check the CSV file looks OK (structure) before sucking it in to the main dataset too.
#
# This over-writes any previously same named file.  Not an issue for this project - the CSV files come time stamped. in the file name
#

import exchangelib
import os
import config

creds = exchangelib.Credentials(config.email, config.pwd)
account = exchangelib.Account(email, credentials=creds, autodiscover=True)
folder = exchangelib.Folder(parent=account.inbox, name="Submitted Data")

submittedFolder = account.inbox / 'Submitted Data'
processedFolder = account.inbox / 'Processed Data'

for msg in submittedFolder.all().order_by('-datetime_received')[:100]:
    moveMail = False

    for attachment in msg.attachments:
	extension = os.path.splitext(attachment.name)[1]
	if (extension == ".csv"):
        	fpath = os.path.join('/a/file/file/path/to save attachment/', attachment.name)
        	with open(fpath, 'wb') as f:
            		f.write(attachment.content)
			moveMail = True
    if (moveMail):    	
	msg.move(processedFolder)
