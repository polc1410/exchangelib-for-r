#! /usr/bin/env python

import exchangelib
import os
import config
import email

def send_email(subject, body, recipients, attachments=""):
    """
    Send an email.

    Parameters
    ----------
    subject : str
    body : str
    recipients : list of str
        Each str is and email adress
    attachments : list of paths or ""

    Examples
    --------
    >>> send_email('Subject line', 'Hello!', ['info@example.com'])
    """
    # Load account from config file
    creds = exchangelib.Credentials(config.email, config.pwd)
    account = exchangelib.Account(primary_smtp_address = config.email, credentials=creds, autodiscover=True)

    to_recipients = []
    for recipient in recipients:
        to_recipients.append(exchangelib.Mailbox(email_address=recipient))
    # Create message
    m = exchangelib.Message(account=account,
                folder=account.sent,
                subject=subject,
                body=body,
                to_recipients=to_recipients)

    # attach files
    for attachment in attachments or []:
	if (attachment != ""):
	    with open(attachment, "rb") as f:
	    	attachment_content = f.read()
            head, tail = os.path.split(attachment)
            file = exchangelib.FileAttachment(name=tail, content=attachment_content)
            m.attach(file)
    m.send_and_save()


# Send email
#send_email(account, 'Test 14:35', 'works', ['example@example.org.uk'],
#           attachments=attachments)

#send_email("Test 13:03", "Works", ['example@example.org.uk'])
